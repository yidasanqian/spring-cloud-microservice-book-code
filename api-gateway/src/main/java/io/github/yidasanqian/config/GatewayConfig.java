package io.github.yidasanqian.config;

import io.github.yidasanqian.filter.AccessFilter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.cloud.netflix.zuul.filters.discovery.PatternServiceRouteMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfig {

    @Bean
    public AccessFilter accessFilter() {
        return new AccessFilter();
    }

    @Bean
    public PatternServiceRouteMapper patternServiceRouteMapper() {
        // 自定义路由规则匹配：/user-service-v1/** -> /v1/user-service/**,不匹配则使用默认规则
        return new PatternServiceRouteMapper("(?<name>^.+)-(?<version>)v.+$",
                "${version}/${name}");
    }

    // 动态化配置
    @RefreshScope
    @ConfigurationProperties("zuul")
    public ZuulProperties zuulProperties() {
        return new ZuulProperties();
    }
}
