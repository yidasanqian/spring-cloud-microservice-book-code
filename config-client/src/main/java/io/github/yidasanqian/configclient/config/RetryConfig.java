package io.github.yidasanqian.configclient.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.interceptor.RetryOperationsInterceptor;

@Configuration
public class RetryConfig {

    /**
     * 配置客户端重试
     *
     * @return
     */
    @Bean
    public RetryOperationsInterceptor configServerRetryInterceptor() {
        return new RetryOperationsInterceptor();
    }
}
