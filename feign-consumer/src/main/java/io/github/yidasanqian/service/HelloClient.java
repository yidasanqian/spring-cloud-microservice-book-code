package io.github.yidasanqian.service;

import io.github.yidasanqian.config.FeignLogConfiguration;
import io.github.yidasanqian.helloserviceapi.service.IHelloService;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(name = "hello-service", fallback = HelloServiceFallback.class, configuration = {FeignLogConfiguration.class})
public interface HelloClient extends IHelloService {
}
