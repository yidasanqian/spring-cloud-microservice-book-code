package io.github.yidasanqian.service;

import io.github.yidasanqian.domain.User;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @ FeignClient注解封装了Ribbon、RestTemplate和Hystrix.同时创建了Ribbon客户端
 */
@FeignClient(name = "hello-service")
public interface HelloService {

    @GetMapping("/hello")
    String hello();

    @GetMapping("/hello1")
    String hello(@RequestParam("name") String name);

    @GetMapping("/hello2")
    User hello(@RequestHeader("name") String name, @RequestHeader("age") Integer age);

    @PostMapping(value = "/hello3")
    String hello(@RequestBody User user);
}
