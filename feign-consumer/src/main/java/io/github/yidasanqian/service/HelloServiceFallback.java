package io.github.yidasanqian.service;

import io.github.yidasanqian.domain.User;
import org.springframework.stereotype.Component;

/**
 * 实现服务降级处理
 */
@Component
public class HelloServiceFallback implements HelloService {
    @Override
    public String hello() {
        return "hello降级处理";
    }

    @Override
    public String hello(String name) {
        return "hello_name降级处理";

    }

    @Override
    public User hello(String name, Integer age) {
        return new User("yidasanqian", 25);

    }

    @Override
    public String hello(User user) {
        return "hello_user降级处理";
    }
}
