package io.github.yidasanqian.web;

import io.github.yidasanqian.domain.User;
import io.github.yidasanqian.service.HelloClient;
import io.github.yidasanqian.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @Autowired
    private HelloService helloService;

    @Autowired
    private HelloClient helloClient;

    @GetMapping("/feign-consumer")
    public String hello() {
        return helloService.hello();
    }

    @GetMapping("/feign-consumer2")
    public String helloConsumer2() {
        StringBuilder sb = new StringBuilder();
        sb.append(helloService.hello()).append("\n");
        sb.append(helloService.hello("yidasanqian")).append("\n");
        sb.append(helloService.hello("yidasanqian", 30)).append("\n");
        sb.append(helloService.hello(new User("yidasanqian", 30))).append("\n");
        return sb.toString();
    }

    @GetMapping("/feign-consumer3")
    public String helloConsumer3() {
        StringBuilder sb = new StringBuilder();
        sb.append(helloClient.hello("MIMI")).append("\n");
        sb.append(helloClient.hello("MIMI", 20)).append("\n");
        sb.append(helloClient.hello(new io.github.yidasanqian.helloserviceapi.dto.User("MIMI", 20))).append("\n");
        return sb.toString();
    }
}
