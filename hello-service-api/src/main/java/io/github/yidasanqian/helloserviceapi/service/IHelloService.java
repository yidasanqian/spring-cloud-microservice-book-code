package io.github.yidasanqian.helloserviceapi.service;

import io.github.yidasanqian.helloserviceapi.dto.User;
import org.springframework.web.bind.annotation.*;


public interface IHelloService {

    @GetMapping("/hello4")
    String hello(@RequestParam("name") String name);

    @GetMapping("/hello5")
    User hello(@RequestHeader("name") String name, @RequestHeader("age") Integer age);

    @PostMapping(value = "/hello6")
    String hello(@RequestBody User user);
}
