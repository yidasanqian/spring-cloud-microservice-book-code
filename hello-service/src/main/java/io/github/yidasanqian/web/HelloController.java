package io.github.yidasanqian.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.client.serviceregistry.Registration;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
public class HelloController {
    private static final Logger log = LoggerFactory.getLogger(HelloController.class);

    @Resource
    private Registration registration;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String hello() throws Exception {
        // 测试超时触发断路器
//		int sleepTime = new Random().nextInt(3000);
//		log.info("sleepTime:" + sleepTime);
//		Thread.sleep(sleepTime);

        log.info("/hello, host:" + registration.getHost() + ", service_id:" + registration.getServiceId());
        return "Hello World";
    }

    @RequestMapping(value = "/hello1", method = RequestMethod.GET)
    public String hello(@RequestParam String name) {
        log.info("/hello1, host:" + registration.getHost() + ", service_id:" + registration.getServiceId());
        return "Hello " + name;
    }

    @RequestMapping(value = "/hello2", method = RequestMethod.GET)
    public User hello(@RequestHeader String name, @RequestHeader Integer age) {
        log.info("/hello2, host:" + registration.getHost() + ", service_id:" + registration.getServiceId());
        return new User(name, age);
    }

    @RequestMapping(value = "/hello3", method = RequestMethod.POST)
    public String hello(@RequestBody User user) {
        log.info("/hello3, host:" + registration.getHost() + ", service_id:" + registration.getServiceId());
        return "Hello " + user.getName() + ", " + user.getAge();
    }
}
