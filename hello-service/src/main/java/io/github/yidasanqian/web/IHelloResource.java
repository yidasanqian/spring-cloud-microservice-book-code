package io.github.yidasanqian.web;

import io.github.yidasanqian.helloserviceapi.dto.User;
import io.github.yidasanqian.helloserviceapi.service.IHelloService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
public class IHelloResource implements IHelloService {
    private static final Logger log = LoggerFactory.getLogger(IHelloResource.class);

    @Override
    public String hello(@RequestParam("name") String name) {
        // 测试超时触发断路器
        int sleepTime = new Random().nextInt(3000);
        log.info("sleepTime:" + sleepTime);
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "Hello " + name;
    }

    @Override
    public User hello(@RequestHeader("name") String name, @RequestHeader("age") Integer age) {
        return new User(name, age);
    }

    @Override
    public String hello(@RequestBody User user) {
        return "Hello " + user.getName() + ", " + user.getAge();
    }
}
