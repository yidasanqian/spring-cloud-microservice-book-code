package io.github.yidasanqian.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置消息队列、交换器、路由等高级信息
 */
@Configuration
public class RabbitConfig {

    /**
     * 默认routing key和 queue名称相同
     *
     * @return
     */
    @Bean
    public Queue helloQueue() {
        return new Queue("hello");
    }
}
