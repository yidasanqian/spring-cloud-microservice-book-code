package io.github.yidasanqian.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * <p>
 * {@code @RabbitListener}注解，实现对hello队列的监听
 * <p>
 * {@code @RabbitHandler}注解，实现对监听对象的消费
 */
@RabbitListener(queues = "hello")
@Component
public class Receiver {

    @RabbitHandler
    public void process(String recMsg) {
        System.out.println("Receiver.process recMsg:" + recMsg);
    }
}
