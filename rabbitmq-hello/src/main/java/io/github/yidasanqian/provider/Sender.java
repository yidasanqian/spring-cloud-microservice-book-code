package io.github.yidasanqian.provider;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class Sender {

    @Autowired
    private AmqpTemplate amqpTemplate;

    public void send() {
        String message = "hello " + LocalDateTime.now();
        System.out.println("Sender.send message: " + message);
        amqpTemplate.convertAndSend("hello", message);
    }
}
