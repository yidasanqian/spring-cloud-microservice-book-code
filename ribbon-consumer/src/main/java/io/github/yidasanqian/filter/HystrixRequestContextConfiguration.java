package io.github.yidasanqian.filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HystrixRequestContextConfiguration {

    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(new HystrixRequestContextServletFilter());
        // 添加过滤规则.
        filterRegistrationBean.addUrlPatterns("/*");
        return filterRegistrationBean;
    }
}
