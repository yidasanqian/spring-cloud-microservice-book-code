package io.github.yidasanqian.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.github.yidasanqian.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@Service
public class HelloService {

    private static final Logger log = LoggerFactory.getLogger(HelloService.class);


    @Autowired
    RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "helloFallback", commandKey = "HelloService.hello")
    public String hello() {
        long start = System.currentTimeMillis();
        StringBuilder result = new StringBuilder();

        /*
         GET restTemplate url参数映射方式一
          */
        String body1 = restTemplate.getForEntity("http://HELLO-SERVICE/hello", String.class).getBody();
        result.append(body1).append("<br/>");
        String body2 = restTemplate.getForEntity("http://HELLO-SERVICE/hello1?name={1}", String.class, "restTemplate url参数映射方式一").getBody();
        result.append(body2).append("<br/>");

        /*
         GET restTemplate url参数映射方式二
          */
        Map<String, String> params = new HashMap<>();
        params.put("name", "restTemplate url参数映射方式二");
        String body3 = restTemplate.getForEntity("http://HELLO-SERVICE/hello1?name={name}", String.class, params).getBody();
        result.append(body3).append("<br/>");

         /*
         GET restTemplate url参数映射方式三
          */
        UriComponents uriComponents = UriComponentsBuilder.fromUriString("http://HELLO-SERVICE/hello1?name={name}")
                .build()
                .expand("yidasanqian expand")
                .encode();
        URI uri = uriComponents.toUri();
        String body4 = restTemplate.getForEntity(uri, String.class).getBody();
        result.append(body4).append("<br/>");

        long end = System.currentTimeMillis();

        log.info("Spend time : " + (end - start) + "ms");
        return result.toString();
    }

    @HystrixCommand(fallbackMethod = "helloPostFallback", commandKey = "HelloService.helloPost")
    public String helloPost(User user) {
        StringBuilder result = new StringBuilder();
        // POST

        String postResult = restTemplate.postForObject("http://HELLO-SERVICE/hello3", user, String.class);
        result.append("postForObject==>");
        result.append(postResult).append("<br>");

        ResponseEntity<String> responseEntity = restTemplate.postForEntity("http://HELLO-SERVICE/hello3", user, String.class);
        result.append("postForEntity==>");
        result.append(responseEntity.getBody()).append("<br>");

        URI responseURI = restTemplate.postForLocation("http://HELLO-SERVICE/hello3", user);
        result.append("postForLocation==>");
        result.append(responseURI).append("<br>");

        return result.toString();
    }

    public void helloPut(User user) {
        // PUT
        restTemplate.put("http://HELLO-SERVICE/hello4?name={1}", user, "yidasanqian");
    }

    public void helloPatch(User user) {
        // Patch
        String result = restTemplate.patchForObject("http://HELLO-SERVICE/hello5", user, String.class);
    }

    public void helloDelete() {
        // delete
        restTemplate.delete("http://HELLO-SERVICE/hello6?id={1}", "1");
    }

    // 服务降级
    private String helloFallback(Throwable throwable) {
        log.error(throwable.getMessage(), throwable);
        return "error";
    }

    private String helloPostFallback(User user, Throwable throwable) {
        log.error(throwable.getMessage(), throwable);
        return user.toString();
    }

}
