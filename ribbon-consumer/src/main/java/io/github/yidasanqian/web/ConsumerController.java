package io.github.yidasanqian.web;

import io.github.yidasanqian.model.User;
import io.github.yidasanqian.service.HelloService;
import io.github.yidasanqian.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
public class ConsumerController {

    private static final Logger log = LoggerFactory.getLogger(ConsumerController.class);

    @Autowired
    HelloService helloService;

    @Autowired
    UserService userService;

    @RequestMapping(value = "/hello-consumer", method = RequestMethod.GET)
    public String helloConsumer() {
        return helloService.hello();
    }

    @PostMapping("/hello-consumer")
    public String helloPost(@RequestBody User user) {
        return helloService.helloPost(user);
    }

    @GetMapping("/user-consumer")
    public Object userConsumer() {
        return userService.userCommandRequest();
    }

    @GetMapping("/query-user-consumer")
    public Object queryUserConsumer() {
        return userService.userCommandGetRequest();
    }

    @GetMapping("/user-consumer/{id}")
    public Object getUserById(@PathVariable Long id) {
        log.info("第一次请求命令");
        User user = userService.getUserById(id);
        /*log.info("更新缓存命令");
        User user1 = new User(1L, "yidasanqian2", 22);
        userService.update(user1);*/
        log.info("第二次请求命令");
        user = userService.getUserById(id);
        return user;
    }

    @PutMapping("/user-consumer")
    public Object updateUser(@RequestBody User user) {
        userService.update(user);
        return user;
    }

    @GetMapping("/find-user-consumer/{id}")
    public Object queryUser(@PathVariable String id) {
        return userService.find(id);
    }

    @GetMapping("/find-user-consumer")
    public Object queryUsers(@RequestParam("ids") String ids) {
        String[] idArr = StringUtils.commaDelimitedListToStringArray(ids);
        List<String> idList = Arrays.asList(idArr);
        return userService.findAll(idList);
    }
}
