package io.github.yidasanqian.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.messaging.handler.annotation.SendTo;

/**
 * App1 中实现了对input输入通道的监听，并且在接受到消息后，对消息做一些简单的处理，然后
 * 通过{@code @SendTo}把处理方法返回的内容以消息的方式发送到output通道中.
 */
@EnableBinding(Processor.class)
public class App1 {

    private static final Logger log = LoggerFactory.getLogger(App1.class);

    @SendTo(Processor.OUTPUT)
    @StreamListener(Processor.INPUT)
    public Object receiverFromInput(Object payload) {
        log.info("receiverFromInput payload:{}", payload);
        return "From input channel Return - " + payload;
    }

}
