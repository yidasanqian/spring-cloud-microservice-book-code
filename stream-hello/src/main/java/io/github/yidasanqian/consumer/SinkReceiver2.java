package io.github.yidasanqian.consumer;

import io.github.yidasanqian.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.MessageChannel;

/**
 * <p>
 * {@code @EnableBinding}该注解用来指定一个或多个定义了{@code @Input}或{@code @Output}
 * 注解的接口，以此实现对消息通道{@link MessageChannel}的绑定
 * <p>
 * {@link Sink}接口是Spring Cloud Stream中默认实现对额对输入消息通道绑定的定义。
 * <p>
 * {@code @StreamListener}主要注解在方法上，该方法将会注册为消息中间件上数据流的事件监听器，
 * 注解中的属性值对应监听的消息通道名。
 * </p>
 */
//@EnableBinding(Sink.class)
public class SinkReceiver2 {

    private static final Logger log = LoggerFactory.getLogger(SinkReceiver2.class);

    @StreamListener(Sink.INPUT)
    public void receive(User user) {
        log.info("receive user:{}", user);
    }
}
