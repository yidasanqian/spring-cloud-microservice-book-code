package io.github.yidasanqian.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "trace-2")
public interface TraceClient {

    @GetMapping("/trace2")
    String trace();
}
