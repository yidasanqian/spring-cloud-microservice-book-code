package io.github.yidasanqian.web;

import io.github.yidasanqian.client.TraceClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TraceController {

    private static final Logger log = LoggerFactory.getLogger(TraceController.class);

    @Autowired
    private TraceClient traceClient;

    @GetMapping("trace1")
    public String trace() {
        log.info("=== call trace 1 ===");
        return traceClient.trace();
    }
}
