package io.github.yidasanqian.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TraceController {

    private static final Logger log = LoggerFactory.getLogger(TraceController.class);

    @GetMapping("/trace2")
    public String trace() {
        log.info("=== call trace-2 ==");
        return "Trace 2";
    }
}
